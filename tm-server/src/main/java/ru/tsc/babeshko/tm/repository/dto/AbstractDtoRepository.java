package ru.tsc.babeshko.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.babeshko.tm.dto.model.AbstractModelDto;

@NoRepositoryBean
public interface AbstractDtoRepository<M extends AbstractModelDto> extends JpaRepository<M, String> {

}